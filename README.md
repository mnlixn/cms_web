
# 项目介绍
## 启动服务
 npm run serve
## 技术点
    Vue3 + Vue-router + Pinia + Element-plus + axios
## 参考文档
    https://cli.vuejs.org/zh/guide/webpack.html#%E7%AE%80%E5%8D%95%E7%9A%84%E9%85%8D%E7%BD%AE%E6%96%B9%E5%BC%8F
    https://element-plus.org/zh-CN/guide/design.html
## npm设置源信息
    (base) wangwenhui@wangwenhuideMac-mini cms_web % npm install
    (⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂⠂) ⠋ idealTree:cms_web: sill idealTree buildDeps
    设置命令
        npm config set registry https://npm.aliyun.com/
        npm config get registry
## 安装Element-plus
    1. 安装命令
        cnpm install element-plus --save
    2. 引入
        1. 全量导入
            ```js
            // main.ts
            import { createApp } from 'vue'
            import ElementPlus from 'element-plus'
            import 'element-plus/dist/index.css'
            import App from './App.vue'

            const app = createApp(App)

            app.use(ElementPlus)
            app.mount('#app')
            ```
        2. 按需引用
            1. 下载依赖包
                npm install -D unplugin-vue-components unplugin-auto-import
            2. 配置文件
              vue.config.js

            ```js
                const { defineConfig } = require('@vue/cli-service')
                const AutoImport = require('unplugin-auto-import/webpack')
                const Components = require('unplugin-vue-components/webpack')
                const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')

                module.exports = defineConfig({
                transpileDependencies: true,
                configureWebpack: {
                    plugins: [
                    AutoImport({
                        resolvers: [ElementPlusResolver()],
                    }),
                    Components({
                        resolvers: [ElementPlusResolver()],
                    }),
                    ],
                }
                })
            ```
    ## 安装依赖
        1. vue vue-router
        2. npm i normalize.css -S // 对标签简单初始化，保留样式
        3. npm i axios -S
        4. npm i echarts -S
        
## echarts
    1. 安装
        npm install echarts -S
    2. 引入
        按需引用
        ```js
            // 引入 echarts 核心模块，核心模块提供了 echarts 使用必须要的接口。
            import * as echarts from 'echarts/core';
            // 引入柱状图图表，图表后缀都为 Chart
            import { BarChart } from 'echarts/charts';
            // 引入提示框，标题，直角坐标系，数据集，内置数据转换器组件，组件后缀都为 Component
            import {
            TitleComponent,
            TooltipComponent,
            GridComponent,
            DatasetComponent,
            TransformComponent
            } from 'echarts/components';
            // 标签自动布局、全局过渡动画等特性
            import { LabelLayout, UniversalTransition } from 'echarts/features';
            // 引入 Canvas 渲染器，注意引入 CanvasRenderer 或者 SVGRenderer 是必须的一步
            import { CanvasRenderer } from 'echarts/renderers';

            // 注册必须的组件
            echarts.use([
            TitleComponent,
            TooltipComponent,
            GridComponent,
            DatasetComponent,
            TransformComponent,
            BarChart,
            LabelLayout,
            UniversalTransition,
            CanvasRenderer
            ]);

            // 接下来的使用就跟之前一样，初始化图表，设置配置项
            var myChart = echarts.init(document.getElementById('main'));
            myChart.setOption({
            // ...
            });
        ```
## 安装vuex
    npm install vuex@next --save
## 登录页面
    	https://static.zhihu.com/heifetz/assets/logo.e049e9b9.png
## 处理时间格式
    npm install dayjs -S
## element-plus转中文
    1. main.js或者App.vue
    ```js
        import ElementPlus from 'element-plus'
        import locale from 'element-plus/es/locale/lang/zh-cn'
        app.use(ElementPlus, { locale })
    ```
    2. 按需引入转中文 App.vue
    ```js
        <template>
            <el-config-provider :locale="locale">
                <router-view></router-view>
            </el-config-provider>
        </template>
        <script>
        import zhCn from 'element-plus/es/locale/lang/zh-cn'
        import { ElC onfigProvider} from 'element-plus'
        import { reactive } from 'vue'
        const { locale } = reactive({
            locale: zhCn
        })
        </script>
        
    ```
## 接收父组件的数据
    1. const props = defineProps({ 
        pageSize: {
            type: Number,
            default: 10
        },
        total: {
            type: Number,
            default: 10
        }
    })
    2. 转化成响应式
    const {total, pageSize} = toRefs(props);
## 传给父组件
    const emit = defineEmits(['getCurrentPage'])
    emit('getCurrentPage', currentPage);
    getCurrentPage // 父组件定义的方法
## 安装wangEditor
    npm install @wangeditor/editor --save
    npm install @wangeditor/editor-for-vue@next --save