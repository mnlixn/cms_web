import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
    state: () => ({
      isLoggedIn: false,
      userToken: null,
    }),
    actions: {
      login(userToken) {
        this.isLoggedIn = true;
        this.userToken = userToken;
        localStorage.setItem('test_userToken', userToken);
      },
      logout() {
        this.isLoggedIn = false;
        this.userToken = null;
        localStorage.removeItem('test_userToken');
      },
    },
  });