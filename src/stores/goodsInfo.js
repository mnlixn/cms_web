import { defineStore } from 'pinia'

const useGoodsInfoStore = defineStore('goodsInfo', {
  state: () => {
    return {
      title: '添加',
      goodsInfo: {}
    }
  },
  actions: {
    setGoodsInfo(payload) {
      this.title = payload.title;
      this.goodsInfo = payload.goodsInfo;
    }
  }
})

export default useGoodsInfoStore;