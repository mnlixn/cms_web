import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'
import 'normalize.css/normalize.css'
import './assets/css/iconfont.css'
import './assets/css/base.css'
import locale from 'element-plus/es/locale/lang/zh-cn'
    
import api from '@/api'

const app = createApp(App)
// 1. 挂载全局变量
// app.config.globalProperties.$api = api
// 2. provide inject
app.provide('$api', api)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(createPinia())
app.use(ElementPlus, { locale })
app.use(router).mount("#app")


