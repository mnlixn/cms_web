import request  from '@/utils/request'

export function authenticate (username, password) {
  return request({
    url: '/api/login/auth',
    method: 'post',
    data: {
      username: username,
      password: password
    }
  })
}