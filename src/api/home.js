import request from '@/utils/request'

const homeTotalUrl =  '/api/mock/home/total'; // 统计数据
const orderInfoUrl = '/api/mock/home/orderinfo'; // 订单数据
const formatUrl = '/api/mock/home/format'; // 折线图 
export function getHomeTotal () {
    return request({
      url: homeTotalUrl,
      method: 'get'
    })
  }
export function getFormat () {
  return request({
    url: formatUrl,
    method: 'get'
  })
}
export function getOrderInfo () {
  return request({
    url: orderInfoUrl,
    method: 'get'
  })
}