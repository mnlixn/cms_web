import request from '@/utils/request'

const productListUrl =  '/api/mock/product/list'; // 商品列表
const productSearchUrl = '/api/mock/product/search'; // 搜索数据
const productDeleteUrl = '/api/mock/product/delete'; // 删除数据 
const productAddUrl = '/api/mock/product/add'; // 删除数据 

function getProductList () {
  return request({
    url: productListUrl,
    method: 'get'
  })
}
function getProductSearch (params) {
  return request({
    url: productSearchUrl,
    method: 'post',
    data: params
  })
}
function getProductDeleteById (params) {
  return request({
    url: productAddUrl,
    method: 'post',
    data: params
  })
}

function addProduct (params) {
  return request({
    url: productAddUrl,
    method: 'post',
    data: params
  })
}

export default {
  getProductList,
  getProductSearch,
  getProductDeleteById,
  addProduct
}