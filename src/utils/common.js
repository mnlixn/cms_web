// 处理html标签

export function removeHtmlTag(str) {
  return str.replace(/<img.*?>/, "[图片]").replaceAll(/<[^>]+>/g, "").replace(/&nsp;/gi, "")
}